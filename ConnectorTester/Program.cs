﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchNestConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;

namespace ConnectorTester
{
    class Program
    {
        private static clsDBSelector objDBSelector;
        private static clsDBSelector objDBSelector1;
        private static clsCopyIndex objCopyIndex;
        private static clsDataTypes objDataTypes = new clsDataTypes();

        static void Main(string[] args)
        {
            objDBSelector = new clsDBSelector("localhost", 9200, "ontology_db", "reports", 20000, new Guid().ToString());
            objDBSelector1 = new clsDBSelector("localhost", 9200, "ontology_db", "reports", 20000, new Guid().ToString());

            var oList_ClassRel = objDBSelector.get_Data_ClassRel(new List<clsClassRel> { new clsClassRel { ID_Class_Left = "6b94b03a53b24fd98d06451770f8f760" } }, boolOR:true);

            //var olist_LogEntries = objDBSelector.get_Data_Objects(new List<clsOntologyItem>
            //{
            //    new clsOntologyItem
            //    {
            //        GUID_Parent = "351d45912495450182aba425f5235db9"
            //    }
            //});

            //var searchAttributeType = new List<clsOntologyItem>
            //{
            //    new clsOntologyItem
            //    {
            //        Name = "DateTimestamp"
            //    }
            //};

            //var oItemAttributeType = objDBSelector1.get_Data_AttributeType(searchAttributeType).Where(attType => attType.Name == "DateTimestamp").First();


            //var searchAttributes = olist_LogEntries.Select(logEntry => new clsObjectAtt
            //{
            //    ID_Object = logEntry.GUID,
            //    ID_AttributeType = oItemAttributeType.GUID
            //}).ToList();

            //var attributeLists = objDBSelector1.get_Data_ObjectAtt(searchAttributes,boolIDs:false);

            //var searchRleationTypes = olist_LogEntries.Select(logEntry => new clsObjectRel
            //{
            //    ID_Object = logEntry.GUID,
            //    ID_RelationType = "d513be0d5832445ab9e40c5e85818a12",
            //    ID_Parent_Other = "1d9568afb6da49908f4d907dfdd30749"
            //}).ToList();

            //var relationLists = objDBSelector1.get_Data_ObjectRel(searchRleationTypes, boolIDs: false);
            //objCopyIndex = new clsCopyIndex(objDBSelector);
            //objCopyIndex.ClearLists();
            //objCopyIndex.CopyIndex("ontodb", CopyItem.DataTypes | CopyItem.AttributeTypes | CopyItem.Classes | CopyItem.RelationTypes | CopyItem.Objects);
            //objCopyIndex.OList_DataTypes = objDataTypes.DataTypes;
            //objCopyIndex.CopyIndex("ontodb", CopyItem.Objects);
            //objCopyIndex.CopyIndex("ontodb", CopyItem.ClassAttributes);
            //objCopyIndex.CopyIndex("ontodb", CopyItem.AttributeTypes);
            //objCopyIndex.CopyIndex("ontodb", CopyItem.ObjectAttributes);
            //objCopyIndex.CopyIndex("ontodb", CopyItem.ObjectRelations);
        }
    }
}
