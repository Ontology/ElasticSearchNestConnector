﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElasticSearchNestConnector;

namespace ElasticSearchNestConnectorTests
{
    [TestClass]
    public class SelectTests
    {
        [TestMethod]
        public void SelectAttributesAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_AttributeType();

            if (result.Count != TestGlobals.CountAttributeTypes)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectRelationTypesAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_RelationTypes();

            if (result.Count != TestGlobals.CountRelationTypes)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectClassesAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_Classes();

            if (result.Count != TestGlobals.CountClasses)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectObjectsAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_Objects();

            if (result.Count != TestGlobals.CountObjects)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectClassAttributesAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_ClassAtt();

            if (result.Count != TestGlobals.CountClassAtts)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectClassRelationsAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_ClassRel(null);

            if (result.Count != TestGlobals.CountClassRels)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectObjectAttributesAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_ObjectAtt(null);

            if (result.Count != TestGlobals.CountObjectAtts)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectObjectRelationsAll()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);

            var result = dbSelector.get_Data_ObjectRel(null);

            if (result.Count != TestGlobals.CountObjectRels)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SelectAppDocuments()
        {
            var dbSelector = new clsUserAppDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.IndexOutlook, TestGlobals.SearchRange, TestGlobals.Session);
            var documents = dbSelector.GetData_Documents(TestGlobals.IndexOutlook, TestGlobals.TypeOutllok);

        }

        [TestMethod]
        public void SelectSessionDocs()
        {
            var dbSelector = new clsUserAppDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.IndexSession, TestGlobals.SearchRange, TestGlobals.Session);
            var documents = dbSelector.GetData_Documents(TestGlobals.IndexSession, TestGlobals.TypeSession, "Session:a65154f7-8363-1a46-8b9e-569203bc67ef");
        }

        [TestMethod]
        public void GetIndexList()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);
            var indices = dbSelector.IndexList(TestGlobals.Server, TestGlobals.Port);
        }
    }
}
