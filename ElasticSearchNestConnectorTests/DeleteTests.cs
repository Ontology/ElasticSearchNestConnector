﻿using ElasticSearchNestConnector;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchNestConnectorTests
{
    [TestClass]
    public class DeleteTests
    {

        [TestMethod]
        public void DeleteItems()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);
            var dbDeletor = new clsDBDeletor(dbSelector);

            bool fail = false;

            var result = dbDeletor.del_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttBool() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttDateTime() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttInt() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttReal() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttString() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ClassAttType(TestGlobals.NewClass(), TestGlobals.NewAttributeTypeBool());
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_ClassAttType(TestGlobals.NewClass(), TestGlobals.NewAttributeTypeDateTime());
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_ClassAttType(TestGlobals.NewClass(), TestGlobals.NewAttributeTypeInt());
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_ClassAttType(TestGlobals.NewClass(), TestGlobals.NewAttributeTypeReal());
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_ClassAttType(TestGlobals.NewClass(), TestGlobals.NewAttributeTypeString());
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ClassRel(new List<clsClassRel> { TestGlobals.NewClassRel() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_ObjectRel(new List<clsObjectRel> { TestGlobals.NewObjectRel() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_Objects(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewObject() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;
                
            }
            result = dbDeletor.del_Class(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewClass() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;
                
            }
            result = dbDeletor.del_AttributeType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewAttributeTypeBool() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;
                
            }
            result = dbDeletor.del_AttributeType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewAttributeTypeDateTime() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_AttributeType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewAttributeTypeInt() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_AttributeType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewAttributeTypeReal() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }
            result = dbDeletor.del_AttributeType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewAttributeTypeString() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbDeletor.del_RelationType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewRelationType() });

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;
                
            }

            result = dbDeletor.del_AttributeType(new List<OntologyClasses.BaseClasses.clsOntologyItem> { TestGlobals.NewDataType() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            

            

            if (fail)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void DeleteClipboardItems()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);
            var dbDeletor = new clsDBDeletor(dbSelector);

            var result = dbDeletor.del_ObjectRel(new List<clsObjectRel> { TestGlobals.RelClipBelongingObject() });
        }

        [TestMethod]
        public void DeleteAppDocumentItem()
        {
            var dbSelector = new clsUserAppDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.IndexOutlook, TestGlobals.SearchRange, TestGlobals.Session);
            var dbDeletor = new clsUserAppDBDeletor(dbSelector);
            var result = dbDeletor.DeleteItemsById(TestGlobals.IndexOutlook, TestGlobals.TypeOutllok, new List<string> { TestGlobals.NewAppDoc().Id });

        }
    }
}
