﻿using ElasticSearchNestConnector;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchNestConnectorTests
{
    [TestClass]
    public class UpdateTests
    {
        [TestMethod]
        public void InsertNewItems()
        {
            var dbSelector = new clsDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.Index, TestGlobals.IndexRep, TestGlobals.SearchRange, TestGlobals.Session);
            var dbUpdater = new clsDBUpdater(dbSelector);

            var result = dbUpdater.save_DataTypes(new List<clsOntologyItem> { TestGlobals.NewDataType() });
            var fail = false;
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;
                
            }
            result = dbUpdater.save_AttributeType(TestGlobals.NewAttributeTypeBool());

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;
                
            }
            result = dbUpdater.save_AttributeType(TestGlobals.NewAttributeTypeDateTime());

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_AttributeType(TestGlobals.NewAttributeTypeInt());

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_AttributeType(TestGlobals.NewAttributeTypeReal());

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_AttributeType(TestGlobals.NewAttributeTypeString());

            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }

            result = dbUpdater.save_Class(TestGlobals.NewClass());


            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;
                
            }
            result = dbUpdater.save_Objects(new List<clsOntologyItem> { TestGlobals.NewObject() });


            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;
                
            }
            result = dbUpdater.save_RelationType(TestGlobals.NewRelationType());
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }

            result = dbUpdater.save_ClassAtt(new List<clsClassAtt> { TestGlobals.NewClassAttBool() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_ClassAtt(new List<clsClassAtt> { TestGlobals.NewClassAttDateTime() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_ClassAtt(new List<clsClassAtt> { TestGlobals.NewClassAttInt() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_ClassAtt(new List<clsClassAtt> { TestGlobals.NewClassAttReal() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }
            result = dbUpdater.save_ClassAtt(new List<clsClassAtt> { TestGlobals.NewClassAttString() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }

            result = dbUpdater.save_ClassRel(new List<clsClassRel> { TestGlobals.NewClassRel() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID)
            {
                fail = true;

            }

            var result1 = dbUpdater.save_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttBool() });
            if (result1.Result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result1.Result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result1 = dbUpdater.save_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttDateTime() });
            if (result1.Result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result1.Result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result1 = dbUpdater.save_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttInt() });
            if (result1.Result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result1.Result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result1 = dbUpdater.save_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttReal() });
            if (result1.Result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result1.Result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result1 = dbUpdater.save_ObjectAtt(new List<clsObjectAtt> { TestGlobals.NewObjectAttString() });
            if (result1.Result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result1.Result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            result = dbUpdater.save_ObjectRel(new List<clsObjectRel> { TestGlobals.NewObjectRel() });
            if (result.GUID == TestGlobals.LogStates.LogState_Error.GUID || result1.Result.GUID == TestGlobals.LogStates.LogState_Nothing.GUID)
            {
                fail = true;

            }

            if (fail)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InsertAppDocumentItem()
        {
            var dbSelector = new clsUserAppDBSelector(TestGlobals.Server, TestGlobals.Port, TestGlobals.IndexOutlook, TestGlobals.SearchRange, TestGlobals.Session);
            var dbUpdater = new clsUserAppDBUpdater(dbSelector);
            var result = dbUpdater.SaveDoc(new List<clsAppDocuments> { TestGlobals.NewAppDoc() }, TestGlobals.TypeOutllok);
        }
    }
}
