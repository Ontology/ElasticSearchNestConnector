﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using Nest;

namespace ElasticSearchNestConnector
{
    [Flags]
    public enum SortEnum
    {
        ASC_Name = 1,
        DESC_Name = 2,
        NONE = 4,
        ASC_OrderID = 8,
        DESC_OrderID = 16
    }

    public class clsDBSelector
    {
        public string Server { get; private set; }
        public int Port { get; private set; }
        public string Index { get; private set; }
        public string IndexRep { get; private set; }
        public int SearchRange { get; private set; }
        public string Session { get; private set; }
        public List<string> SpecialCharacters_Write { get; set; }
        public List<string> SpecialCharacters_Read { get; set; }

        private SortEnum sort = SortEnum.NONE;

        public IElasticClient ElConnector { get; private set; }

        private ISearchResponse<clsObjectRel> resultObjectRel;
        private ISearchResponse<clsObjectAtt> resultObjectAddOrder;
        private ISearchResponse<clsOntologyItem> resultAttributeType;
        private ISearchResponse<clsOntologyItem> resultAttributeTypeCount;
        private ISearchResponse<clsClassAtt> resultClassAtt;
        private ISearchResponse<clsClassAtt> resultClassAttCount;
        private ISearchResponse<clsOntologyItem> resultClasses;
        private ISearchResponse<clsOntologyItem> resultClassesCount;
        private ISearchResponse<clsClassRel> resultClassRel;
        private ISearchResponse<clsClassRel> resultClassRelCount;
        private ISearchResponse<clsOntologyItem> resultDataTypes;
        private ISearchResponse<clsOntologyItem> resultDataTypesCount;
        private ISearchResponse<clsObjectAtt> resultObjectAtt;
        private ISearchResponse<clsObjectAtt> resultObjectAttCount;
        private ISearchResponse<clsObjectRel> resultObjectRelCount;
        private ISearchResponse<clsOntologyItem> resultObjects;
        private ISearchResponse<clsObjectRel> resultObjectRelTree;
        private ISearchResponse<clsObjectRel> resultObjectRelTreeCount;
        private ISearchResponse<clsOntologyItem> resultObjectCount;
        private ISearchResponse<clsObjectRel> resultObjectRelOrder;
        private ISearchResponse<clsOntologyItem> resultRelationTypes;
        private ISearchResponse<clsOntologyItem> resultRelationTypesCount;

        private List<clsOntologyItem> oList_ObjectsSearch;
        private List<clsOntologyItem> oList_AttributeTypesSearch;
        private List<clsOntologyItem> oList_ClassesSearch;
        private List<clsOntologyItem> oList_DataTypesSearch;
        private List<clsOntologyItem> oList_OtherObjectsSearch;
        private List<clsObjectRel> OList_OhterSearch;

        private clsFields objFields = new clsFields();
        private clsTypes objTypes = new clsTypes();

        public List<clsOntologyItem> OntologyList_Objects1 { get; set; }
        public List<clsOntologyItem> OntologyList_Objects2 { get; set; }
        public List<clsObjectRel> OntologyList_ObjectRel_ID { get; set; }
        public List<clsObjectRel> OntologyList_ObjectRel { get; set; }
        public List<clsObjectTree> OntologyList_ObjectTree { get; set; }
        public List<clsOntologyItem> OntologyList_Classes1 { get; set; }
        public List<clsOntologyItem> OntologyList_Classes2 { get; set; }
        public List<clsOntologyItem> OntologyList_RelationTypes1 { get; set; }
        public List<clsOntologyItem> OntologyList_RelationTypes2 { get; set; }
        public List<clsOntologyItem> OntologyList_AttributTypes1 { get; set; }
        public List<clsOntologyItem> OntologyList_AttributTypes2 { get; set; }
        public List<clsClassRel> OntologyList_ClassRel_ID { get; set; }
        public List<clsClassRel> OntologyList_ClassRel { get; set; }
        public List<clsClassAtt> OntologyList_ClassAtt_ID { get; set; }
        public List<clsClassAtt> OntologyList_ClassAtt { get; set; }
        public List<clsObjectAtt> OntologyList_ObjAtt_ID { get; set; }
        public List<clsObjectAtt> OntologyList_ObjAtt { get; set; }
        public List<clsOntologyItem> OntologyList_DataTypes { get; set; }
        public List<clsAttribute> OntologyList_Attributes { get; set; }

        public IndexExistsDescriptor GetIndexExistsDescriptor(string index)
        {
            return new IndexExistsDescriptor(index);
        }

        public IndexSettings GetIndexSettings()
        {
            return new IndexSettings();
        }

        public DeleteIndexDescriptor GetDeleteIndexDescriptor(string index)
        {
            return new DeleteIndexDescriptor(index);
        }

        public SortEnum Sort
        {
            get
            {
                return sort;
            }
            set
            {
                sort = value;
            }


        }

        public List<string> create_Query_ClassRel(List<clsClassRel> OList_ClassRel, bool boolClear = true)
        {
            var queryList = new List<string>();

            var queryRows = new List<QueryRow>();

            if (OList_ClassRel != null)
            {
                OList_ClassRel.ForEach(clsRel =>
                {
                    var queryItems = new List<QueryItem>();
                    if (!string.IsNullOrEmpty(clsRel.ID_Class_Left))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Class_Left,
                            Value = clsRel.ID_Class_Left
                        };
                        queryItems.Add(queryItem);
                    }

                    if (!string.IsNullOrEmpty(clsRel.ID_Class_Right))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Class_Right,
                            Value = clsRel.ID_Class_Right
                        };
                        queryItems.Add(queryItem);
                    }

                    if (!string.IsNullOrEmpty(clsRel.ID_RelationType))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_RelationType,
                            Value = clsRel.ID_RelationType
                        };
                        queryItems.Add(queryItem);
                    }

                    if (!string.IsNullOrEmpty(clsRel.Ontology))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.String,
                            Field = objFields.Ontology,
                            Value = clsRel.Ontology
                        };
                        queryItems.Add(queryItem);
                    }

                    if (clsRel.Min_Forw != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Long,
                            Field = objFields.Min_Forw,
                            Value = clsRel.Min_Forw
                        };
                        queryItems.Add(queryItem);
                    }

                    if (clsRel.Max_Forw != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Long,
                            Field = objFields.Max_Forw,
                            Value = clsRel.Max_Forw
                        };
                        queryItems.Add(queryItem);
                    }

                    if (clsRel.Max_Backw != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Long,
                            Field = objFields.Max_Backw,
                            Value = clsRel.Max_Backw
                        };
                        queryItems.Add(queryItem);
                    }

                    if (queryItems.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems,
                            ItemOperator = QueryOperator.AND
                        });
                    }
                });


                


            }

            if (queryRows.Any())
            {
                int ix = 0;
                int count = 0;
                while (ix < queryRows.Count)
                {
                    if (ix + 1000 < queryRows.Count)
                    {
                        count = 1000;
                    }
                    else
                    {
                        count = queryRows.Count - ix;
                    }
                    queryList.Add(string.Join(" " + Enum.GetName(typeof(QueryOperator), QueryOperator.OR) + " ",
                        queryRows.GetRange(ix, count).Select(queryRow => queryRow.RowQuery)));
                    ix += count;
                }
            }
            else
            {
                queryList.Add("*");
            }


            queryRows.Clear();
            return queryList;

        }

        public List<string> create_Query_ObjectAtt(List<clsObjectAtt> OList_ObjectAtt, bool doJoin = false, bool doJoinClasses = false)
        {

            var queryList = new List<string>();

            var queryRows = new List<QueryRow>();

            if (OList_ObjectAtt != null)
            {
                

                OList_ObjectAtt.ForEach(objAtt =>
                {
                    var queryItems = new List<QueryItem>();
                    if (!string.IsNullOrEmpty(objAtt.ID_Attribute))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Attribute,
                            Value = objAtt.ID_Attribute
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objAtt.ID_AttributeType))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_AttributeType,
                            Value = objAtt.ID_AttributeType
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objAtt.ID_Class))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Class,
                            Value = objAtt.ID_Class
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objAtt.ID_DataType))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_DataType,
                            Value = objAtt.ID_DataType
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objAtt.ID_Object))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Object,
                            Value = objAtt.ID_Object
                        };
                        queryItems.Add(queryItem);
                    }
                    if (objAtt.OrderID != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Long,
                            Field = objFields.OrderID,
                            Value = objAtt.OrderID
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objAtt.Val_String))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.String,
                            Field = objFields.Val_String,
                            Value = objAtt.Val_String
                        };
                        queryItems.Add(queryItem);
                    }
                    if (objAtt.Val_Bit != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Bool,
                            Field = objFields.Val_Bool,
                            Value = objAtt.Val_Bool
                        };
                        queryItems.Add(queryItem);
                    }
                    if (objAtt.Val_Date != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.DateTime,
                            Field = objFields.Val_Datetime,
                            Value = objAtt.Val_Date,
                            RangeForDateTime = new TimeSpan(0,0,1)
                        };
                        queryItems.Add(queryItem);
                    }
                    if (objAtt.Val_Double != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Double,
                            Field = objFields.Val_Real,
                            Value = objAtt.Val_Real
                        };
                        queryItems.Add(queryItem);
                    }
                    if (objAtt.Val_Lng != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Long,
                            Field = objFields.Val_Int,
                            Value = objAtt.Val_Int
                        };
                        queryItems.Add(queryItem);
                    }

                    if (queryItems.Any())
                    {
                       
                        queryRows.Add( new QueryRow
                        {
                            QueryItems = queryItems,
                            ItemOperator = QueryOperator.AND
                        });
                    }
                });
            }

            if (queryRows.Any())
            {
                int ix = 0;
                int count = 0;
                while (ix < queryRows.Count)
                {
                    if (ix + 1000 < queryRows.Count)
                    {
                        count = 1000;
                    }
                    else
                    {
                        count = queryRows.Count - ix;
                    }
                    queryList.Add(string.Join(" " + Enum.GetName(typeof (QueryOperator), QueryOperator.OR) + " ",
                        queryRows.GetRange(ix, count).Select(queryRow => queryRow.RowQuery)));
                    ix += count;
                }
            }
            else
            {
                queryList.Add("*");
            }
            
            
            queryRows.Clear();
            return queryList;
        }

        public List<string> create_Query_ObjectRel(List<clsObjectRel> OList_ObjectRel)
        {
            var queryList = new List<string>();

            var queryRows = new List<QueryRow>();

            if (OList_ObjectRel != null)
            {


                OList_ObjectRel.ForEach(objRel =>
                {
                    var queryItems = new List<QueryItem>();
                    if (!string.IsNullOrEmpty(objRel.ID_Object))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Object,
                            Value = objRel.ID_Object
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objRel.ID_Parent_Object))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Parent_Object,
                            Value = objRel.ID_Parent_Object
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objRel.ID_Other))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Other,
                            Value = objRel.ID_Other
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objRel.ID_Parent_Other))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_Parent_Other,
                            Value = objRel.ID_Parent_Other
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objRel.ID_RelationType))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.ID_RelationType,
                            Value = objRel.ID_RelationType
                        };
                        queryItems.Add(queryItem);
                    }
                    if (!string.IsNullOrEmpty(objRel.Ontology))
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Id,
                            Field = objFields.Ontology,
                            Value = objRel.Ontology
                        };
                        queryItems.Add(queryItem);
                    }
                    if (objRel.OrderID != null)
                    {
                        var queryItem = new QueryItem(SpecialCharacters_Read)
                        {
                            ValueType = ValueType.Long,
                            Field = objFields.OrderID,
                            Value = objRel.OrderID
                        };
                        queryItems.Add(queryItem);
                    }
                    

                    if (queryItems.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems,
                            ItemOperator = QueryOperator.AND
                        });
                    }
                });
            }

            if (queryRows.Any())
            {
                int ix = 0;
                int count = 0;
                while (ix < queryRows.Count)
                {
                    if (ix + 1000 < queryRows.Count)
                    {
                        count = 1000;
                    }
                    else
                    {
                        count = queryRows.Count - ix;
                    }
                    queryList.Add(string.Join(" " + Enum.GetName(typeof(QueryOperator), QueryOperator.OR) + " ",
                        queryRows.GetRange(ix, count).Select(queryRow => queryRow.RowQuery)));
                    ix += count;
                }
            }
            else
            {
                queryList.Add("*");
            }


            queryRows.Clear();
            return queryList;
            

        }

        public string create_Query_ClassAtt(List<clsClassAtt> OList_ClassAtts = null)
        {
            var strQuery = "";

            var queryRows = new List<QueryRow>();
            if (OList_ClassAtts != null)
            {
                
                
                for (var i = 0; i < OList_ClassAtts.Count; i++)
                {
                    var queryItems = new List<QueryItem>();

                    if (!string.IsNullOrEmpty(OList_ClassAtts[i].ID_AttributeType))
                    {
                        queryItems.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = objFields.ID_AttributeType,
                            Value = OList_ClassAtts[i].ID_AttributeType,
                            ValueType = ValueType.Id
                        });
                    }

                    if (!string.IsNullOrEmpty(OList_ClassAtts[i].ID_Class))
                    {
                        queryItems.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = objFields.ID_Class,
                            Value = OList_ClassAtts[i].ID_Class,
                            ValueType = ValueType.Id
                        });
                    }

                    if (!string.IsNullOrEmpty(OList_ClassAtts[i].ID_DataType))
                    {
                        queryItems.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = objFields.ID_DataType,
                            Value = OList_ClassAtts[i].ID_DataType,
                            ValueType = ValueType.Id
                        });
                    }

                    if (OList_ClassAtts[i].Min != null)
                    {
                        queryItems.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = objFields.ID_DataType,
                            Value = OList_ClassAtts[i].Min.ToString(),
                            ValueType = ValueType.Long
                        });
                    }

                    if (OList_ClassAtts[i].Max != null)
                    {
                        queryItems.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = objFields.ID_DataType,
                            Value = OList_ClassAtts[i].Min.ToString(),
                            ValueType = ValueType.Long
                        });
                    }

                    if (queryItems.Any())
                    {
                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems,
                            ItemOperator = QueryOperator.AND
                        });    
                    }
                    
                }
                
            }

            if (queryRows.Any())
            {
                strQuery = string.Join(" " + QueryOperator.OR + " ", queryRows.Select(queryRow => queryRow.RowQuery));

            }
            if (strQuery == "")
            {
                strQuery = "*";
            }

            return strQuery;
        }
        public string create_Query_ClassAtt(List<clsOntologyItem> OList_Classes = null,
                                                       List<clsOntologyItem> OList_AttributeTypes = null)
        {
            var strQuery = "";

            if (OList_Classes != null)
            {
                if (OList_Classes.Any())
                {
                    

                    var oL_ID = (from obj in OList_Classes
                                 where obj.GUID != null
                                 group obj by obj.GUID
                                     into g
                                     select g.Key).ToList();


                    if (oL_ID.Any())
                    {
                        if (strQuery != "") strQuery += "  AND  ";
                        strQuery += "(";
                        for (var i = 0; i < oL_ID.Count;i++ )
                        {
                            if (i>0) strQuery += " OR ";

                            strQuery += objFields.ID_Class + ":" + oL_ID[i];
                        }
                        strQuery += ")";
                    }
                    

                }
            }

            if (OList_AttributeTypes != null)
            {
                if (OList_AttributeTypes.Any())
                {

                    var oL_ID = (from obj in OList_AttributeTypes
                                 where obj.GUID != null
                                 group obj by obj.GUID
                                     into g
                                     select g.Key).ToList();


                    if (oL_ID.Any())
                    {
                        if (strQuery != "") strQuery += "  AND  ";
                        strQuery += "(";
                        for (var i = 0; i < oL_ID.Count; i++ )
                        {
                            if (i > 0)
                                strQuery += " OR ";

                            strQuery += objFields.ID_AttributeType + ":" + oL_ID[i];
                        }
                        strQuery += ")";
                    }
                    

                }
            }

            if (strQuery == "")
            {
                strQuery = "*";
            }

            return strQuery;
        }

        public string create_Query_Att_OrderID(clsOntologyItem OItem_Object = null,
                                                      clsOntologyItem OItem_AttributeType = null)
        {

            var strQuery = "";

            if (OItem_Object != null)
            {
                if (OItem_Object.GUID != null)
                {
                    strQuery = objFields.ID_Object + ":" + OItem_Object.GUID;
                }


                

                if (OItem_Object.GUID_Parent != null)
                {
                    if (strQuery != "")
                    {
                        strQuery += " AND ";
                    }
                    strQuery += objFields.ID_Class + ":" + OItem_Object.GUID_Parent;
                }

            }


            
            if (OItem_AttributeType != null)
            {
                if (strQuery != "")
                {
                    strQuery += " AND ";
                }

                if (OItem_AttributeType.GUID != null)
                {
                    strQuery += objFields.ID_AttributeType + ":" + OItem_AttributeType.GUID;
                }


            }

            return strQuery;
        }

        public string create_Query_Rel_OrderID(clsOntologyItem OItem_Left = null,
                                                      clsOntologyItem OItem_Right = null,
                                                      clsOntologyItem OItem_RelationType = null)
        {
            var strQuery = "";

            if (OItem_Left != null)
            {

                if (OItem_Left.GUID != null)
                {
                    strQuery = objFields.ID_Object + ":" + OItem_Left.GUID;
                }

                if (OItem_Left.GUID_Parent != null)
                {
                    if (strQuery != "")
                    {
                        strQuery += " AND ";
                    }
                    strQuery += objFields.ID_Parent_Object + ":" + OItem_Left.GUID_Parent;
                }

            }

            if (OItem_Right != null)
            {

                if (OItem_Right.GUID != null)
                {
                    if (strQuery != "")
                    {
                        strQuery += " AND ";
                    }
                    strQuery += objFields.ID_Other + ":" + OItem_Right.GUID;
                }

                if (OItem_Right.GUID_Parent != null)
                {
                    if (strQuery != "")
                    {
                        strQuery += " AND ";
                    }
                    strQuery += objFields.ID_Parent_Other + ":" + OItem_Right.GUID_Parent;
                }


                if (OItem_Right.Type != null)
                {
                    if (strQuery != "")
                    {
                        strQuery += " AND ";
                    }
                    strQuery += objFields.Ontology + ":" + OItem_Right.Type;
                }
            }

            if (OItem_RelationType != null)
            {

                if (OItem_RelationType.GUID != null)
                {
                    if (strQuery != "")
                    {
                        strQuery += " AND ";
                    }
                    strQuery += objFields.ID_RelationType + ":" + OItem_RelationType.GUID;
                }



            }

            return strQuery;
        }

        public string create_Query_Simple(List<clsOntologyItem> OList_Items, string strOntology, bool boolExact = false)
        {
            var strQuery = "";
            var strField_IDParent = "";

            if (strOntology == objTypes.AttributeType)
            {
                strField_IDParent = objFields.ID_DataType;
            }
            else if (strOntology == objTypes.ObjectType)
            {
                strField_IDParent = objFields.ID_Class;
            }
            else
            {
                strField_IDParent = objFields.ID_Parent;
            }

            if (OList_Items != null)
            {
                if (OList_Items.Any())
                {
                    var boolID = false;
                    var oL_ID = (from obj in OList_Items
                                 where obj.GUID != null && obj.GUID != ""
                                 group obj by obj.GUID
                                     into g
                                     select g.Key).ToList();

                    if (oL_ID.Any())
                    {
                        if (strQuery != "") strQuery += "  AND  ";
                        strQuery += "(";
                        for (var i = 0; i < oL_ID.Count;i++ )
                        {
                            if (i>0) strQuery += " OR ";

                            strQuery += objFields.ID_Item + ":" + oL_ID[i];
                        }
                        strQuery += ")";
                    }

                    

                    if (strQuery != "")
                    {
                        boolID = true;
                    }

                    if (!boolID)
                    {
                        strQuery = "";
                        var oL_Name = (from obj in OList_Items
                                       where obj.Name != null && obj.Name != ""
                                       group obj by obj.Name
                                           into g
                                           select g.Key).ToList();


                        if (oL_Name.Any())
                        {
                            if (strQuery != "") strQuery += "  AND  ";
                            strQuery += "(";
                            for (var i = 0; i < oL_Name.Count; i++ )
                            {
                                if (i > 0) strQuery += " OR ";
                                var nameQuery = oL_Name[i];
                                foreach (var specialCharacter in SpecialCharacters_Read)
                                {
                                    nameQuery = nameQuery.Replace(specialCharacter, "\\" + specialCharacter);
                                }

                                if (!boolExact)
                                {
                                    if (nameQuery == oL_Name[i])
                                    {
                                        strQuery += objFields.Name_Item + ":*" + nameQuery + "*";
                                    }
                                    else
                                    {
                                        strQuery += objFields.Name_Item + ":\"" +  nameQuery + "\"";
                                    }


                                }
                                else
                                {
                                    strQuery += objFields + ":" + nameQuery;
                                }

                            }
                            strQuery += ")";
                        }
                        


                        if (strOntology == objTypes.AttributeType ||
                            strOntology == objTypes.ClassType ||
                            strOntology == objTypes.ObjectType)
                        {
                            var oL_IDParent = (from obj in OList_Items
                                               where obj.GUID_Parent != null & obj.GUID_Parent != ""
                                               group obj by obj.GUID_Parent
                                                   into g
                                                   select g.Key).ToList();


                            if (oL_IDParent.Any())
                            {
                                if (strQuery != "") strQuery += "  AND  ";
                                strQuery += "(";
                                for (var i = 0; i < oL_IDParent.Count;i++ )
                                {
                                    if (i > 0) strQuery += " OR ";

                                    strQuery += strField_IDParent + ":" + oL_IDParent[i];
                                }
                                strQuery += ")";
                            }
                            


                        }
                    }
                }
            }

            if (strQuery == "")
            {
                strQuery = "*";
            }

            return strQuery;
        }

        private void initialize_Client()
        {
            var uri = new Uri("http://" + Server + ":" + Port.ToString());//.Purify();

            var settings = new ConnectionSettings(uri).DefaultIndex(Index);
            settings.DefaultTypeNameInferrer(i => i.Name);
            settings.DefaultFieldNameInferrer(p => p);
            ElConnector = new ElasticClient(settings);
            try
            {
                var indexList = IndexList(Server, Port);
                if (indexList.All(ix => ix != IndexRep))
                {
                    try
                    {
                        ElConnector.CreateIndex(IndexRep);

                    }
                    catch (Exception ex)
                    {

                        throw new Exception("Report index!");
                    }
                }
            }
            catch (Exception)
            {
                        
                throw new Exception("Indexes not found!");
            }
            
            
        }

        public long get_Data_Att_OrderByVal(string strOrderField,
                                            clsOntologyItem OItem_Object = null,
                                            clsOntologyItem OItem_AttributeType = null,
                                            bool doASC = true)
        {

            resultObjectAddOrder = null;
            var strQuery = create_Query_Att_OrderID(OItem_Object, OItem_AttributeType);

            long lngOrderID = 0;

            if (doASC)
            {
                var query = new QueryStringQuery();
                query.Query = strQuery;

                var searchRequest = new SearchRequest<clsObjectAtt>(Indices.Index(Index), Types.Type(objTypes.ObjectAtt));
                searchRequest.Query = query;
                searchRequest.From = 0;
                searchRequest.Size = 1;
                searchRequest.Sort = new List<ISort>
                {
                    new SortField {Field =  strOrderField, Order = SortOrder.Ascending}
                };

                resultObjectAddOrder = ElConnector.Search<clsObjectAtt>(searchRequest);
                if (resultObjectAddOrder.Documents.Any())
                {
                    lngOrderID = (long)(typeof(clsObjectAtt).GetProperty(strOrderField).GetValue(resultObjectAddOrder.Documents.First(), null));
                }
            }
            else
            {
                var query = new QueryStringQuery();
                query.Query = strQuery;

                var searchRequest = new SearchRequest<clsObjectAtt>(Indices.Index(Index), Types.Type(objTypes.ObjectAtt));
                searchRequest.Query = query;
                searchRequest.From = 0;
                searchRequest.Size = 1;
                searchRequest.Sort = new List<ISort>
                {
                    new SortField {Field =  strOrderField, Order = SortOrder.Descending}
                };

                resultObjectAddOrder = ElConnector.Search<clsObjectAtt>(searchRequest);

                if (resultObjectAddOrder.Documents.Any())
                {
                    lngOrderID = (long)(typeof(clsObjectAtt).GetProperty(strOrderField).GetValue(resultObjectAddOrder.Documents.First(), null));
                }
            }
            
            return lngOrderID;
        }

        public long get_Data_AttributeTypeCount(List<clsOntologyItem> OList_AttType = null)
        {
            resultAttributeTypeCount = null;
            OntologyList_AttributTypes1.Clear();

            var strQuery = create_Query_Simple(OList_AttType, objTypes.AttributeType);

            var query = new QueryStringQuery();
            query.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.AttributeType));
            searchRequest.Query = query;
            searchRequest.From = 0;
            searchRequest.Size = 1;
            
            resultObjectAddOrder = ElConnector.Search<clsObjectAtt>(searchRequest);


            var lngCount = resultAttributeTypeCount.Total;
            return lngCount;
        }

        public List<clsOntologyItem> get_Data_AttributeType(List<clsOntologyItem> OList_AttType = null, bool List2 = false)
        {
            resultAttributeType = null;
            if (OntologyList_AttributTypes1 == null)
            {
                OntologyList_AttributTypes1 = new List<clsOntologyItem>();
            }

            if (OntologyList_AttributTypes2 == null)
            {
                OntologyList_AttributTypes2 = new List<clsOntologyItem>();
            }

            if (!List2)
            {
                OntologyList_AttributTypes1.Clear();
            }
            else
            {
                OntologyList_AttributTypes2.Clear();
            }


            var strQuery = create_Query_Simple(OList_AttType, objTypes.AttributeType);
            var query = new QueryStringQuery();
            query.Query = strQuery;

            

            var intCount = SearchRange;
            var intPos = 0;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.AttributeType));
            searchRequest.Query = query;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;

            while (intCount > 0)
            {
                intCount = 0;



                if (scrollId == null)
                {
                    resultAttributeType = ElConnector.Search<clsOntologyItem>(searchRequest);
                    scrollId = resultAttributeType.ScrollId;
                }
                else
                {
                    resultObjects = ElConnector.Scroll<clsOntologyItem>("10s", scrollId);
                }
                
                    //ElConnector.Search(Index, objTypes.AttributeType, objBoolQuery.ToString(), intPos,
                                                       //SearchRange);

                if (!List2)
                {
                    OntologyList_AttributTypes1.AddRange(resultAttributeType.Documents.Select(d => new clsOntologyItem 
                        {
                            GUID = d.GUID,
                            Name = d.Name,
                            GUID_Parent = d.GUID_Parent,
                            Type = objTypes.AttributeType
                        }));                
                }
                else
                {
                    OntologyList_AttributTypes2.AddRange(resultAttributeType.Documents);
                }


                if (resultAttributeType.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultAttributeType.Documents.Count();
                    intPos += SearchRange;
                }



            }
            if (!List2)
            {
                return OntologyList_AttributTypes1;
            }
            else
            {
                return OntologyList_AttributTypes2;
            }

        }

        public long get_Data_ClassAttCount(List<clsClassAtt> OList_ClassAtts)
        {
            resultClassAttCount = null;
            var strQuery = create_Query_ClassAtt(OList_ClassAtts);

            var query = new QueryStringQuery();
            query.Query = strQuery;

            var searchRequest = new SearchRequest<clsClassAtt>(Indices.Index(Index), Types.Type(objTypes.ClassAtt));
            searchRequest.Query = query;
            searchRequest.From = 0;
            searchRequest.Size = 1;

            resultClassAttCount = ElConnector.Search<clsClassAtt>(searchRequest);

            //ElConnector.Search(Index, objTypes.ClassAtt, objBoolQuery.ToString(), 0, 1);

            var lngCount = resultClassAttCount.Total;

            return lngCount;
        }

        public long get_Data_ClassAttCount(List<clsOntologyItem> OList_Class = null,
                                           List<clsOntologyItem> OList_AttributeType = null)
        {
            resultClassAttCount = null;
            var strQuery = create_Query_ClassAtt(OList_Class, OList_AttributeType);

            var query = new QueryStringQuery();
            query.Query = strQuery;

            var searchRequest = new SearchRequest<clsClassAtt>(Indices.Index(Index), Types.Type(objTypes.ClassAtt));
            searchRequest.Query = query;
            searchRequest.From = 0;
            searchRequest.Size = 1;

            resultClassAttCount = ElConnector.Search<clsClassAtt>(searchRequest);

                //ElConnector.Search(Index, objTypes.ClassAtt, objBoolQuery.ToString(), 0, 1);

            var lngCount = resultClassAttCount.Total;

            return lngCount;
        }

        public List<clsClassAtt> get_Data_ClassAtt(List<clsClassAtt> OList_ClassAtts, bool boolIDs = true)
        {
            resultClassAtt = null;
            if (OntologyList_ClassAtt_ID == null)
            {
                OntologyList_ClassAtt_ID = new List<clsClassAtt>();
            }

            if (OntologyList_ClassAtt == null)
            {
                OntologyList_ClassAtt = new List<clsClassAtt>();
            }

            OntologyList_ClassAtt_ID.Clear();
            OntologyList_ClassAtt.Clear();

            var strQuery = create_Query_ClassAtt(OList_ClassAtts);

            var query = new QueryStringQuery();
            query.Query = strQuery;

            var intCount = SearchRange;
            var intPos = 0;

            var searchRequest = new SearchRequest<clsClassAtt>(Indices.Index(Index), Types.Type(objTypes.ClassAtt));
            searchRequest.Query = query;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;

            while (intCount > 0)
            {
                
                if (scrollId == null)
                {
                    resultClassAtt = ElConnector.Search<clsClassAtt>(searchRequest);
                    scrollId = resultClassAtt.ScrollId;
                }
                else
                {
                    resultClassAtt = ElConnector.Scroll<clsClassAtt>("10s", scrollId);
                }
                

                //ElConnector.Search(Index, objTypes.ClassAtt, objBoolQuery.ToString(), intPos, SearchRange);


                OntologyList_ClassAtt_ID.AddRange(resultClassAtt.Documents);

                if (resultClassAtt.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultClassAtt.Documents.Count();
                    intPos += SearchRange;
                }
            }

            if (!boolIDs)
            {
                var oList_AttributeTypes = (from objAttributeType in OntologyList_ClassAtt_ID
                                            group objAttributeType by objAttributeType.ID_AttributeType
                                                into g
                                                select new clsOntologyItem { GUID = g.Key }).ToList();
                get_Data_AttributeType(oList_AttributeTypes);

                var oList_Classes = (from objClass in OntologyList_ClassAtt_ID
                                     group objClass by objClass.ID_Class
                                         into g
                                         select new clsOntologyItem { GUID = g.Key }).ToList();

                get_Data_Classes(oList_Classes);

                var oList_DataTypes = (from objDataType in OntologyList_AttributTypes1
                                       group objDataType by objDataType.GUID_Parent
                                           into g
                                           select new clsOntologyItem { GUID = g.Key }).ToList();

                get_Data_DataTypes(oList_DataTypes);

                OntologyList_ClassAtt.AddRange((from objClassAtt in OntologyList_ClassAtt_ID
                                                join objClass in OntologyList_Classes1 on objClassAtt.ID_Class equals objClass.GUID
                                                join objAttType in OntologyList_AttributTypes1 on objClassAtt.ID_AttributeType equals objAttType.GUID
                                                join objDataType in OntologyList_DataTypes on objAttType.GUID_Parent equals objDataType.GUID
                                                select new clsClassAtt
                                                {
                                                    ID_AttributeType = objAttType.GUID,
                                                    ID_Class = objClass.GUID,
                                                    ID_DataType = objClassAtt.ID_DataType,
                                                    Name_AttributeType = objAttType.Name,
                                                    Name_Class = objClass.Name,
                                                    Name_DataType = objDataType.Name,
                                                    Min = objClassAtt.Min,
                                                    Max = objClassAtt.Max
                                                }));
            }



            if (boolIDs)
            {
                return OntologyList_ClassAtt_ID;
            }
            else
            {
                return OntologyList_ClassAtt;
            }
        }

        public List<clsClassAtt> get_Data_ClassAtt(List<clsOntologyItem> OList_Class = null,
                                           List<clsOntologyItem> OList_AttributeType = null,
                                           bool boolIDs = true)
        {
            resultClassAtt = null;
            if (OntologyList_ClassAtt_ID == null)
            {
                OntologyList_ClassAtt_ID = new List<clsClassAtt>();
            }

            if (OntologyList_ClassAtt == null)
            {
                OntologyList_ClassAtt = new List<clsClassAtt>();
            }

            OntologyList_ClassAtt_ID.Clear();
            OntologyList_ClassAtt.Clear();

            var strQuery = create_Query_ClassAtt(OList_Class, OList_AttributeType);

            var intCount = SearchRange;
            var intPos = 0;

            var searchRequest = new SearchRequest<clsClassAtt>(Indices.Index(Index), Types.Type(objTypes.ClassAtt));
            var query = new QueryStringQuery();
            query.Query = strQuery;

            searchRequest.Query = query;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;

            while (intCount > 0)
            {

               

                
                if (scrollId == null)
                {
                    resultClassAtt = ElConnector.Search<clsClassAtt>(searchRequest);
                    scrollId = resultClassAtt.ScrollId;
                }
                else
                {
                    resultObjects = ElConnector.Scroll<clsOntologyItem>("10s", scrollId);
                }
                
                    //ElConnector.Search(Index, objTypes.ClassAtt, objBoolQuery.ToString(), intPos, SearchRange);
                
                
                OntologyList_ClassAtt_ID.AddRange(resultClassAtt.Documents);

                if (resultClassAtt.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultClassAtt.Documents.Count();
                    intPos += SearchRange;
                }
            }

            if (!boolIDs)
            {
                var oList_AttributeTypes = (from objAttributeType in OntologyList_ClassAtt_ID
                                            group objAttributeType by objAttributeType.ID_AttributeType
                                                into g
                                                select new clsOntologyItem { GUID = g.Key }).ToList();
                get_Data_AttributeType(oList_AttributeTypes);

                var oList_Classes = (from objClass in OntologyList_ClassAtt_ID
                                     group objClass by objClass.ID_Class
                                         into g
                                         select new clsOntologyItem { GUID = g.Key }).ToList();

                get_Data_Classes(oList_Classes);

                var oList_DataTypes = (from objDataType in OntologyList_AttributTypes1
                                       group objDataType by objDataType.GUID_Parent
                                           into g
                                           select new clsOntologyItem { GUID = g.Key }).ToList();

                get_Data_DataTypes(oList_DataTypes);

                OntologyList_ClassAtt.AddRange((from objClassAtt in OntologyList_ClassAtt_ID
                                                join objClass in OntologyList_Classes1 on objClassAtt.ID_Class equals objClass.GUID
                                                join objAttType in OntologyList_AttributTypes1 on objClassAtt.ID_AttributeType equals objAttType.GUID
                                                join objDataType in OntologyList_DataTypes on objAttType.GUID_Parent equals objDataType.GUID
                                                select new clsClassAtt
                                                {
                                                    ID_AttributeType = objAttType.GUID,
                                                    ID_Class = objClass.GUID,
                                                    ID_DataType = objClassAtt.ID_DataType,
                                                    Name_AttributeType = objAttType.Name,
                                                    Name_Class = objClass.Name,
                                                    Name_DataType = objDataType.Name,
                                                    Min = objClassAtt.Min,
                                                    Max = objClassAtt.Max
                                                }));
            }


            if (boolIDs)
            {
                return OntologyList_ClassAtt_ID;
            }
            else
            {
                return OntologyList_ClassAtt;
            }

        }

        public long get_Data_ClassRelCount(List<clsClassRel> OList_ClassRel)
        {
            resultClassRelCount = null;
            var queries = create_Query_ClassRel(OList_ClassRel);
            long lngCount = 0;
            foreach (var query in queries)
            {
                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<clsClassRel>(Indices.Index(Index), Types.Type(objTypes.ClassRel));
                searchRequest.Query = queryItem;
                searchRequest.From = 0;
                searchRequest.Size = 1;

                resultClassRelCount = ElConnector.Search<clsClassRel>(searchRequest);


                lngCount += resultObjectRelCount.Total;
            }

            return lngCount;
        }

        public List<clsClassRel> get_Data_ClassRel(List<clsClassRel> OList_ClassRel,
                                                   bool boolIDs = true,
                                                   bool boolOR = false)
        {
            resultClassRel = null;
            if (OntologyList_ClassRel_ID == null)
            {
                OntologyList_ClassRel_ID = new List<clsClassRel>();
            }

            if (OntologyList_ClassRel == null)
            {
                OntologyList_ClassRel = new List<clsClassRel>();
            }

            if (OntologyList_Classes1 == null)
            {
                OntologyList_Classes1 = new List<clsOntologyItem>();
            }

            if (OntologyList_Classes2 == null)
            {
                OntologyList_Classes2 = new List<clsOntologyItem>();
            }

            if (OntologyList_RelationTypes1 == null)
            {
                OntologyList_RelationTypes1 = new List<clsOntologyItem>();
            }

            OntologyList_ClassRel_ID.Clear();
            OntologyList_ClassRel.Clear();
            OntologyList_Classes1.Clear();
            OntologyList_Classes2.Clear();
            OntologyList_RelationTypes1.Clear();

            switch (sort)
            {
            }

            var queries = create_Query_ClassRel(OList_ClassRel);

            foreach (var query in queries)
            {
                var intCount = SearchRange;
                var intPos = 0;

                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<clsClassRel>(Indices.Index(Index), Types.Type(objTypes.ClassRel));
                searchRequest.Query = queryItem;
                searchRequest.From = intPos;
                searchRequest.Size = SearchRange;
                searchRequest.Scroll = "10s";
                string scrollId = null;

                while (intCount > 0)
                {
                    
                    if (scrollId == null)
                    {
                        resultClassRel = ElConnector.Search<clsClassRel>(searchRequest);
                        scrollId = resultClassRel.ScrollId;
                    }
                    else
                    {
                        resultClassRel = ElConnector.Scroll<clsClassRel>("10s", scrollId);
                    }

                    OntologyList_ClassRel_ID.AddRange(resultClassRel.Documents);

                    if (resultClassRel.Documents.Count() < SearchRange)
                    {
                        intCount = 0;
                    }
                    else
                    {
                        intCount = resultClassRel.Documents.Count();
                        intPos += SearchRange;
                    }
                }

            }
                

            if (!boolIDs)
            {
                var oList_ClassesLeft = (from objClass in OntologyList_ClassRel_ID
                                         group objClass by objClass.ID_Class_Left
                                             into g
                                             select new clsOntologyItem { GUID = g.Key }).ToList();

                get_Data_Classes(oList_ClassesLeft);

                var oList_ClassesRight = (from objClass in OntologyList_ClassRel_ID
                                          group objClass by objClass.ID_Class_Right
                                              into g
                                              select new clsOntologyItem { GUID = g.Key }).ToList();

                if (oList_ClassesRight.Any())
                {
                    get_Data_Classes(oList_ClassesRight, true);
                }

                var oList_RelationTypes = (from objClass in OntologyList_ClassRel_ID
                                           group objClass by objClass.ID_RelationType
                                               into g
                                               select new clsOntologyItem { GUID = g.Key }).ToList();

                get_Data_RelationTypes(oList_RelationTypes);

                if (boolOR)
                {
                    OntologyList_ClassRel.AddRange((from objClassRel in OntologyList_ClassRel_ID
                                                    join objClassLeft in OntologyList_Classes1 on objClassRel.ID_Class_Left equals objClassLeft.GUID
                                                    join objClassRight in OntologyList_Classes2 on objClassRel.ID_Class_Right equals objClassRight.GUID into objClassesRight
                                                    from objClassRight in objClassesRight.DefaultIfEmpty()
                                                    where objClassRight == null
                                                    join objRelationType in OntologyList_RelationTypes1 on objClassRel.ID_RelationType equals objRelationType.GUID
                                                    select new clsClassRel
                                                    {
                                                        ID_Class_Left = objClassLeft.GUID,
                                                        Name_Class_Left = objClassLeft.Name,
                                                        ID_RelationType = objRelationType.GUID,
                                                        Name_RelationType = objRelationType.Name,
                                                        Min_Forw = objClassRel.Min_Forw,
                                                        Max_Forw = objClassRel.Max_Forw,
                                                        Max_Backw = objClassRel.Max_Backw,
                                                        Ontology = objClassRel.Ontology
                                                    }).ToList());
                }
                else
                {
                    OntologyList_ClassRel.AddRange((from objClassRel in OntologyList_ClassRel_ID
                                                    join objClassLeft in OntologyList_Classes1 on objClassRel.ID_Class_Left equals objClassLeft.GUID
                                                    join objClassRight in OntologyList_Classes2 on objClassRel.ID_Class_Right equals objClassRight.GUID
                                                    join objRelationType in OntologyList_RelationTypes1 on objClassRel.ID_RelationType equals objRelationType.GUID
                                                    select new clsClassRel
                                                    {
                                                        ID_Class_Left = objClassLeft.GUID,
                                                        Name_Class_Left = objClassLeft.Name,
                                                        ID_Class_Right = objClassRight.GUID,
                                                        Name_Class_Right = objClassRight.Name,
                                                        ID_RelationType = objRelationType.GUID,
                                                        Name_RelationType = objRelationType.Name,
                                                        Min_Forw = objClassRel.Min_Forw,
                                                        Max_Forw = objClassRel.Max_Forw,
                                                        Max_Backw = objClassRel.Max_Backw,
                                                        Ontology = objClassRel.Ontology
                                                    }).ToList());
                }


            }


            if (boolIDs)
            {
                return OntologyList_ClassRel_ID;
            }
            else
            {
                return OntologyList_ClassRel;
            }

        }

        public long get_Data_ClassesCount(List<clsOntologyItem> OList_Classes = null)
        {

            resultClassesCount = null;
            var strQuery = create_Query_Simple(OList_Classes, objTypes.ClassType);

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.ClassType));
            searchRequest.Query = queryItem;
            searchRequest.From = 0;
            searchRequest.Size = 1;

            resultClassesCount = ElConnector.Search<clsOntologyItem>(searchRequest);


            var lngCount = resultClassesCount.Total;

            resultClassesCount = null;
            return lngCount;
        }

        public List<clsOntologyItem> get_Data_Classes(List<clsOntologyItem> OList_Classes = null,
                                                  bool boolClasses_Right = false,
                                                  string strSort = null)
        {
            resultClasses = null;
            var strQuery = create_Query_Simple(OList_Classes, objTypes.ClassType);

            if (OntologyList_Classes1 == null)
            {
                OntologyList_Classes1 = new List<clsOntologyItem>();
            }

            if (OntologyList_Classes2 == null)
            {
                OntologyList_Classes2 = new List<clsOntologyItem>();
            }

            if (!boolClasses_Right)
            {
                OntologyList_Classes1.Clear();
            }
            else
            {
                OntologyList_Classes2.Clear();
            }


            var intCount = SearchRange;
            var intPos = 0;

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.ClassType));
            searchRequest.Query = queryItem;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;

            while (intCount > 0)
            {
                

                if (scrollId == null)
                {
                    resultClasses = ElConnector.Search<clsOntologyItem>(searchRequest);
                    scrollId = resultClasses.ScrollId;
                }
                else
                {
                    resultClasses = ElConnector.Scroll<clsOntologyItem>("10s", scrollId);
                }
                
                

                if (!boolClasses_Right)
                {

                    OntologyList_Classes1.AddRange(resultClasses.Documents.Select(d => new clsOntologyItem 
                        {
                            GUID = d.GUID,
                            Name = d.Name,
                            GUID_Parent = d.GUID_Parent,
                            Type = objTypes.ClassType
                        }));


                }
                else
                {
                    OntologyList_Classes2.AddRange(resultClasses.Documents.Select(d => new clsOntologyItem
                    {
                        GUID = d.GUID,
                        Name = d.Name,
                        GUID_Parent = d.GUID_Parent,
                        Type = objTypes.ClassType
                    }));
                }


                if (resultClasses.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultClasses.Documents.Count();
                    intPos += SearchRange;
                }
            }

            if (!boolClasses_Right)
            {
                return OntologyList_Classes1;
            }
            else
            {
                return OntologyList_Classes2;
            }

        }

        public long get_Data_DataTypesCount(List<clsOntologyItem> OList_DataTypes = null)
        {
            resultDataTypesCount = null;
            OntologyList_AttributTypes1.Clear();

            var strQuery = create_Query_Simple(OList_DataTypes, objTypes.DataType);

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.DataType));
            searchRequest.Query = queryItem;
            searchRequest.From = 0;
            searchRequest.Size = 1;

            resultDataTypesCount = ElConnector.Search<clsOntologyItem>(searchRequest);

            var lngCount = resultDataTypesCount.Total;

            return lngCount;
        }

        public List<clsOntologyItem> get_Data_DataTypes(List<clsOntologyItem> OList_DataType = null)
        {
            resultDataTypes = null;
            if (OntologyList_DataTypes == null)
            {
                OntologyList_DataTypes = new List<clsOntologyItem>();
            }
            OntologyList_DataTypes.Clear();

            var strQuery = create_Query_Simple(OList_DataType, objTypes.DataType);

            var intCount = SearchRange;
            var intPos = 0;

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.DataType));
            searchRequest.Query = queryItem;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;

            while (intCount > 0)
            {
                intCount = 0;

                

                if (scrollId == null)
                {
                    resultDataTypes = ElConnector.Search<clsOntologyItem>(searchRequest);
                    scrollId = resultDataTypes.ScrollId;
                }
                else
                {
                    resultDataTypes = ElConnector.Scroll<clsOntologyItem>("10s", scrollId);
                }
                



                OntologyList_DataTypes.AddRange(resultDataTypes.Documents);

                if (resultDataTypes.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultDataTypes.Documents.Count();
                    intPos += SearchRange;
                }



            }

            return OntologyList_DataTypes;
        }

        public long get_Data_ObjectAttCount(List<clsObjectAtt> OList_ObjectAtt)
        {
            resultObjectAttCount = null;

            if (OntologyList_AttributTypes1 == null)
            {
                OntologyList_AttributTypes1 = new List<clsOntologyItem>();
            }
            OntologyList_AttributTypes1.Clear();

            var queries = create_Query_ObjectAtt(OList_ObjectAtt);

            long lngCount = 0;
            foreach (var query in queries)
            {
                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<clsObjectAtt>(Indices.Index(Index), Types.Type(objTypes.ObjectAtt));
                searchRequest.Query = queryItem;
                searchRequest.From = 0;
                searchRequest.Size = 1;

                resultObjectAttCount = ElConnector.Search<clsObjectAtt>(searchRequest);


                lngCount += resultObjectAttCount.Total;
            }

            return lngCount;
        }

        public List<clsObjectAtt> get_Data_ObjectAtt(List<clsObjectAtt> OList_ObjectAtt,
                                                     bool boolIDs = true,
                                                     bool doJoin = false,
                                                     bool doJoinClasses = false)
        {
            resultObjectAtt = null;
            var queries = create_Query_ObjectAtt(OList_ObjectAtt, doJoin, doJoinClasses);

            if (OntologyList_ObjAtt == null) OntologyList_ObjAtt = new List<clsObjectAtt>();
            if (OntologyList_ObjAtt_ID == null) OntologyList_ObjAtt_ID = new List<clsObjectAtt>();
            if (OntologyList_AttributTypes1 == null) OntologyList_AttributTypes1 = new List<clsOntologyItem>();
            if (OntologyList_Attributes == null) OntologyList_Attributes = new List<clsAttribute>();
            if (OntologyList_Classes1 == null) OntologyList_Classes1 = new List<clsOntologyItem>();
            if (OntologyList_Objects1 == null) OntologyList_Objects1 = new List<clsOntologyItem>();
            if (OntologyList_DataTypes == null) OntologyList_DataTypes = new List<clsOntologyItem>();

            OntologyList_ObjAtt.Clear();
            OntologyList_ObjAtt_ID.Clear();
            OntologyList_AttributTypes1.Clear();
            OntologyList_Attributes.Clear();
            OntologyList_Classes1.Clear();
            if (doJoin == false)
                OntologyList_Objects1.Clear();

            OntologyList_DataTypes.Clear();



            foreach (var query in queries)
            {
                var intCount = SearchRange;
                var intPos = 0;

                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<clsObjectAtt>(Indices.Index(Index), Types.Type(objTypes.ObjectAtt));
                searchRequest.Query = queryItem;
                searchRequest.From = intPos;
                searchRequest.Size = SearchRange;
                searchRequest.Scroll = "10s";

                string scrollId = null;

                while (intCount > 0)
                {
                    intCount = 0;

                    

                    if (scrollId == null)
                    {
                        resultObjectAtt = ElConnector.Search<clsObjectAtt>(searchRequest);
                        scrollId = resultObjectAtt.ScrollId;
                    }
                    else
                    {
                        resultObjectAtt = ElConnector.Scroll<clsObjectAtt>("10s", scrollId);
                    }
                    


                    OntologyList_ObjAtt_ID.AddRange(from doc in resultObjectAtt.Documents
                        join oldDoc in OntologyList_ObjAtt_ID on doc.ID_Attribute equals oldDoc.ID_Attribute into
                            oldDocs
                        from oldDoc in oldDocs.DefaultIfEmpty()
                        where oldDoc == null
                        select doc);

                    if (resultObjectAtt.Documents.Count() < SearchRange)
                    {
                        intCount = 0;
                    }
                    else
                    {
                        intCount = resultObjectAtt.Documents.Count();
                        intPos += SearchRange;
                    }



                }
            }

            if (!boolIDs)
            {
                if (!doJoin)
                {
                    oList_ObjectsSearch = (from objObj in OntologyList_ObjAtt_ID
                                            group objObj by objObj.ID_Object
                                                into g
                                                select
                                                    new clsOntologyItem { GUID = g.Key, Type = objTypes.ObjectType }).ToList();
                    var number1 = oList_ObjectsSearch.Count;
                    var curPos1 = 0;
                    while (curPos1 < number1)
                    {
                        var count = number1 - curPos1 > 500 ? 500 : number1 - curPos1;
                        get_Data_Objects(oList_ObjectsSearch.GetRange(curPos1, count), ClearObj1: false);
                        curPos1 += count;
                    }

                }

                oList_AttributeTypesSearch = (from objAttType in OntologyList_ObjAtt_ID
                                                group objAttType by objAttType.ID_AttributeType
                                                    into g
                                                    select new clsOntologyItem { GUID = g.Key }).ToList();

                if (oList_AttributeTypesSearch.Any())
                {
                    get_Data_AttributeType(oList_AttributeTypesSearch);
                }

                oList_ClassesSearch = (from objClass in OntologyList_ObjAtt_ID
                                        group objClass by objClass.ID_Class
                                            into g
                                            select new clsOntologyItem { GUID = g.Key }).ToList();

                if (oList_ClassesSearch.Any())
                {
                    get_Data_Classes(oList_ClassesSearch);
                }

                oList_DataTypesSearch = (from objDataType in OntologyList_ObjAtt_ID
                                            group objDataType by objDataType.ID_DataType
                                                into g
                                                select new clsOntologyItem { GUID = g.Key }).ToList();

                if (oList_DataTypesSearch.Any())
                {
                    get_Data_DataTypes(oList_DataTypesSearch);
                }

                OntologyList_ObjAtt = (from objObjAtt in OntologyList_ObjAtt_ID
                                        join objObject in OntologyList_Objects1 on objObjAtt.ID_Object equals
                                            objObject.GUID
                                        join objClass in OntologyList_Classes1 on objObject.GUID_Parent equals
                                            objClass.GUID
                                        join objAttType in OntologyList_AttributTypes1 on objObjAtt.ID_AttributeType
                                            equals objAttType.GUID
                                        join objDataType in OntologyList_DataTypes on objAttType.GUID_Parent equals
                                            objDataType.GUID
                                        select new clsObjectAtt
                                        {
                                            ID_Attribute = objObjAtt.ID_Attribute,
                                            ID_AttributeType = objAttType.GUID,
                                            ID_Class = objClass.GUID,
                                            ID_Object = objObject.GUID,
                                            ID_DataType = objDataType.GUID,
                                            OrderID = objObjAtt.OrderID,
                                            Name_AttributeType = objAttType.Name,
                                            Name_Class = objClass.Name,
                                            Name_Object = objObject.Name,
                                            Name_DataType = objDataType.Name,
                                            Val_Bit = objObjAtt.Val_Bit,
                                            Val_Date = objObjAtt.Val_Date,
                                            Val_Double = objObjAtt.Val_Double,
                                            Val_Lng = objObjAtt.Val_Lng,
                                            Val_Named = objObjAtt.Val_Named,
                                            Val_String = objObjAtt.Val_String
                                        }).ToList();
            }

            if (boolIDs)
            {
                return OntologyList_ObjAtt_ID;
            }
            else
            {
                return OntologyList_ObjAtt;
            }

        }

        public long get_Data_ObjectsCount(List<clsOntologyItem> OList_Objects = null)
        {
            resultObjectCount = null;
            var strQuery = create_Query_Simple(OList_Objects, objTypes.ObjectType);

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.ObjectType));
            searchRequest.Query = queryItem;
            searchRequest.From = 0;
            searchRequest.Size = 1;

            resultObjectCount = ElConnector.Search<clsOntologyItem>(searchRequest);

            var lngCount = resultObjectCount.Total;

            return lngCount;
        }

        public List<clsOntologyItem> get_Data_Objects(List<clsOntologyItem> OList_Objects = null,
                                                      bool List2 = false,
                                                      bool ClearObj1 = true,
                                                      bool ClearObj2 = true,
                                                      bool boolExact = false)
        {
            resultObjects = null;
            if (OntologyList_Objects1 == null)
            {
                OntologyList_Objects1 = new List<clsOntologyItem>();
            }
            else
            {
                if (ClearObj1) OntologyList_Objects1.Clear();
            }
            if (OntologyList_Objects2 == null)
            {
                OntologyList_Objects2 = new List<clsOntologyItem>();
            }
            else
            {
                if (ClearObj2) OntologyList_Objects2.Clear();
            }

            var strQuery = create_Query_Simple(OList_Objects, objTypes.ObjectType, boolExact);

            var intCount = SearchRange;
            var intPos = 0;
            string scrollId = null;

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.ObjectType));
            searchRequest.Query = queryItem;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            while (intCount > 0)
            {
                intCount = 0;

                if (scrollId == null)
                {
                    resultObjects = ElConnector.Search<clsOntologyItem>(searchRequest);
                    scrollId = resultObjects.ScrollId;
                }
                else
                {
                    resultObjects = ElConnector.Scroll<clsOntologyItem>("10s", scrollId);
                }
                

                if (!List2)
                {
                    OntologyList_Objects1.AddRange(resultObjects.Documents.Select(d => new clsOntologyItem
                    {
                        GUID = d.GUID,
                        Name = d.Name,
                        GUID_Parent = d.GUID_Parent,
                        Type = objTypes.ObjectType
                    }));
                }
                else
                {
                    OntologyList_Objects2.AddRange(resultObjects.Documents.Select(d => new clsOntologyItem
                    {
                        GUID = d.GUID,
                        Name = d.Name,
                        GUID_Parent = d.GUID_Parent,
                        Type = objTypes.ObjectType
                    }));
                }


                if (resultObjects.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultObjects.Documents.Count();
                    intPos += SearchRange;
                }


            }

            if (!List2)
            {
                return OntologyList_Objects1;
            }
            else
            {
                return OntologyList_Objects2;
            }

        }

        public long get_Data_ObjectRelCount(List<clsObjectRel> OList_ObjectRel = null)
        {

            resultObjectRelCount = null;
            var queries = create_Query_ObjectRel(OList_ObjectRel);
            long lngCount = 0;
            foreach (var query in queries)
            {
                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<clsObjectRel>(Indices.Index(Index), Types.Type(objTypes.ObjectRel));
                searchRequest.Query = queryItem;
                searchRequest.From = 0;
                searchRequest.Size = 1;

                resultObjectRelCount = ElConnector.Search<clsObjectRel>(searchRequest);

                lngCount += resultObjectRelCount.Total;    
            }

            return lngCount;
        }

        public List<clsObjectRel> get_Data_ObjectRel(List<clsObjectRel> OList_ObjectRel = null,
                                                     bool boolIDs = true,
                                                     bool doJoin_Left = false,
                                                     bool doJoin_Right = false,
                                                     bool doClear = true)
        {
            resultObjectRel = null;
            var queries = create_Query_ObjectRel(OList_ObjectRel);

            if (OntologyList_ObjectRel == null || doClear)
            {
                OntologyList_ObjectRel = new List<clsObjectRel>();    
            }

            
            OntologyList_ObjectRel_ID = new List<clsObjectRel>();
            
            OntologyList_Classes1 = new List<clsOntologyItem>();
            OntologyList_RelationTypes1 = new List<clsOntologyItem>();
            OntologyList_DataTypes = new List<clsOntologyItem>();

            if (OntologyList_Objects1 == null)
            {
                OntologyList_Objects1 = new List<clsOntologyItem>();
            }
            else
            {
                if (!doJoin_Left && doClear) OntologyList_Objects1.Clear();
            }

            if (OntologyList_Objects2 == null)
            {
                OntologyList_Objects2 = new List<clsOntologyItem>();
            }
            else
            {
                if (!doJoin_Right && doClear) OntologyList_Objects2.Clear();
            }


            string strSort = null;
            if (sort == SortEnum.ASC_OrderID)
            {
                strSort = "OrderID:asc";
            }
            else if (sort == SortEnum.DESC_OrderID)
            {
                strSort = "OrderID:desc";
            }

            foreach (var query in queries)
            {
                var intCount = SearchRange;
                var intPos = 0;

                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<clsObjectRel>(Indices.Index(Index), Types.Type(objTypes.ObjectRel));
                searchRequest.Query = queryItem;
                searchRequest.From = intPos;
                searchRequest.Size = SearchRange;
                searchRequest.Scroll = "10s";
                if (sort == SortEnum.ASC_OrderID)
                {
                    searchRequest.Sort = new List<ISort>
                    {
                        new SortField { Field =  objFields.OrderID, Order = SortOrder.Ascending}
                    };
                }
                else if (sort == SortEnum.DESC_OrderID)
                {
                    searchRequest.Sort = new List<ISort>
                    {
                        new SortField { Field =  objFields.OrderID, Order = SortOrder.Descending}
                    };
                    
                }

                string scrollId = null;


                while (intCount > 0)
                {
                    intCount = 0;

                    if (scrollId == null)
                    {
                        resultObjectRel = ElConnector.Search<clsObjectRel>(searchRequest);
                        scrollId = resultObjectRel.ScrollId;
                    }
                    else
                    {
                        resultObjectRel = ElConnector.Scroll<clsObjectRel>("10s", scrollId);
                    }


                    OntologyList_ObjectRel_ID.AddRange(from doc in resultObjectRel.Documents
                                                       join oldDoc in OntologyList_ObjectRel_ID on new
                                                       {
                                                           ID_Object = doc.ID_Object,
                                                           ID_Other = doc.ID_Other,
                                                           ID_RelationType = doc.ID_RelationType
                                                       } equals
                                                                                                   new
                                                                                                   {
                                                                                                       ID_Object = oldDoc.ID_Object,
                                                                                                       ID_Other = oldDoc.ID_Other,
                                                                                                       ID_RelationType = oldDoc.ID_RelationType
                                                                                                   } into oldDocs
                                                       from oldDoc in oldDocs.DefaultIfEmpty()
                                                       where oldDoc == null
                                                       select doc);

                    if (resultObjectRel.Documents.Count() < SearchRange)
                    {
                        intCount = 0;
                    }
                    else
                    {
                        intCount = resultObjectRel.Documents.Count();
                        intPos += SearchRange;
                    }








                }    
            }
            


            if (!boolIDs)
            {
                if (!doJoin_Left)
                {
                    oList_ObjectsSearch = (from objObj in OntologyList_ObjectRel_ID
                                           group objObj by objObj.ID_Object
                                               into g
                                               select
                                                   new clsOntologyItem { GUID = g.Key, Type = objTypes.ObjectType }).ToList();
                    var number1 = oList_ObjectsSearch.Count;
                    var curPos1 = 0;
                    while (curPos1 < number1)
                    {
                        var count = number1 - curPos1 > 500 ? 500 : number1 - curPos1;
                        get_Data_Objects(oList_ObjectsSearch.GetRange(curPos1,count),ClearObj1:false);
                        curPos1 += count;
                    }
                    //if (OntologyList_ObjectRel_ID.Count < 1000)
                    //{
                        
                    //}
                    //else
                    //{
                    //    oList_ObjectsSearch = (from objObj in OntologyList_ObjectRel_ID
                    //                    group objObj by objObj.ID_Parent_Object
                    //                        into g
                    //                        select
                    //                            new clsOntologyItem { GUID_Parent = g.Key, Type = objTypes.ObjectType }).ToList();
                    //}


                    //if (oList_ObjectsSearch.Any())
                    //{
                    //    get_Data_Objects(oList_ObjectsSearch);
                    //}
                }


                oList_OtherObjectsSearch = (from objClass in OntologyList_ObjectRel_ID
                                            where objClass.Ontology == objTypes.ObjectType
                                            group objClass by objClass.ID_Other
                                                into g
                                                select new clsOntologyItem { GUID = g.Key }).ToList();
                var number2 = oList_OtherObjectsSearch.Count;
                var curPos2 = 0;
                while (curPos2 < number2)
                {
                    var count = number2 - curPos2 > 500 ? 500 : number2 - curPos2;
                    OntologyList_Objects2 = oList_OtherObjectsSearch.Any() ? get_Data_Objects(oList_OtherObjectsSearch.GetRange(curPos2,count), List2: true, ClearObj2: false, ClearObj1: false ) : new List<clsOntologyItem>();
                    curPos2 += count;
                }

                get_Data_Classes();

                get_Data_RelationTypes();

                get_Data_AttributeType();

                OntologyList_DataTypes = get_Data_DataTypes();

                OList_OhterSearch = (from objOther in OntologyList_ObjectRel_ID
                                   where objOther.Ontology == objTypes.AttributeType ||
                                         objOther.Ontology == objTypes.RelationType ||
                                         objOther.Ontology == objTypes.ClassType
                                   select objOther).ToList();

                if (OntologyList_Objects2.Any())
                {
                    OntologyList_ObjectRel.AddRange((from objObjRel in OntologyList_ObjectRel_ID
                                                     join objLeft in OntologyList_Objects1 on objObjRel.ID_Object equals
                                                         objLeft.GUID
                                                     join objLeftClass in OntologyList_Classes1 on
                                                         objObjRel.ID_Parent_Object equals objLeftClass.GUID
                                                     join objRight in OntologyList_Objects2 on objObjRel.ID_Other equals
                                                         objRight.GUID
                                                     join objRightParent in OntologyList_Classes1 on
                                                         objObjRel.ID_Parent_Other equals objRightParent.GUID
                                                     join objRelationType in OntologyList_RelationTypes1 on
                                                         objObjRel.ID_RelationType equals objRelationType.GUID
                                                     select new clsObjectRel
                                                     {
                                                         ID_Object = objLeft.GUID,
                                                         Name_Object = objLeft.Name,
                                                         ID_Parent_Object = objLeftClass.GUID,
                                                         Name_Parent_Object = objLeftClass.Name,
                                                         ID_Other = objRight.GUID,
                                                         Name_Other = objRight.Name,
                                                         ID_Parent_Other = objRightParent.GUID,
                                                         Name_Parent_Other = objRightParent.Name,
                                                         OrderID = objObjRel.OrderID,
                                                         Ontology = objObjRel.Ontology,
                                                         ID_RelationType = objRelationType.GUID,
                                                         Name_RelationType = objRelationType.Name
                                                     }).ToList());

                }

                if (OList_OhterSearch.Where(p => p.Ontology == objTypes.AttributeType).Any())
                {
                    OntologyList_ObjectRel.AddRange((from objObjRel in OntologyList_ObjectRel_ID
                                                     join objLeft in OntologyList_Objects1 on objObjRel.ID_Object equals
                                                         objLeft.GUID
                                                     join objLeftClass in OntologyList_Classes1 on
                                                         objObjRel.ID_Parent_Object equals objLeftClass.GUID
                                                     join objRight in OntologyList_AttributTypes1 on objObjRel.ID_Other equals
                                                         objRight.GUID
                                                     join objRightParent in OntologyList_DataTypes on
                                                         objRight.GUID_Parent equals objRightParent.GUID
                                                     join objRelationType in OntologyList_RelationTypes1 on
                                                         objObjRel.ID_RelationType equals objRelationType.GUID
                                                     select new clsObjectRel
                                                     {
                                                         ID_Object = objLeft.GUID,
                                                         Name_Object = objLeft.Name,
                                                         ID_Parent_Object = objLeftClass.GUID,
                                                         Name_Parent_Object = objLeftClass.Name,
                                                         ID_Other = objRight.GUID,
                                                         Name_Other = objRight.Name,
                                                         ID_Parent_Other = objRightParent.GUID,
                                                         Name_Parent_Other = objRightParent.Name,
                                                         OrderID = objObjRel.OrderID,
                                                         Ontology = objObjRel.Ontology,
                                                         ID_RelationType = objRelationType.GUID,
                                                         Name_RelationType = objRelationType.Name
                                                     }).ToList());

                }

                if (OList_OhterSearch.Where(p => p.Ontology == objTypes.RelationType).Any())
                {
                    OntologyList_ObjectRel.AddRange((from objObjRel in OntologyList_ObjectRel_ID
                                                     join objLeft in OntologyList_Objects1 on objObjRel.ID_Object equals
                                                         objLeft.GUID
                                                     join objLeftClass in OntologyList_Classes1 on
                                                         objObjRel.ID_Parent_Object equals objLeftClass.GUID
                                                     join objRight in OntologyList_RelationTypes1 on objObjRel.ID_Other equals
                                                         objRight.GUID
                                                     join objRelationType in OntologyList_RelationTypes1 on
                                                         objObjRel.ID_RelationType equals objRelationType.GUID
                                                     select new clsObjectRel
                                                     {
                                                         ID_Object = objLeft.GUID,
                                                         Name_Object = objLeft.Name,
                                                         ID_Parent_Object = objLeftClass.GUID,
                                                         Name_Parent_Object = objLeftClass.Name,
                                                         ID_Other = objRight.GUID,
                                                         Name_Other = objRight.Name,
                                                         OrderID = objObjRel.OrderID,
                                                         Ontology = objObjRel.Ontology,
                                                         ID_RelationType = objRelationType.GUID,
                                                         Name_RelationType = objRelationType.Name
                                                     }).ToList());

                }

                if (OList_OhterSearch.Where(p => p.Ontology == objTypes.ClassType).Any())
                {
                    OntologyList_ObjectRel.AddRange((from objObjRel in OntologyList_ObjectRel_ID
                                                     join objLeft in OntologyList_Objects1 on objObjRel.ID_Object equals
                                                         objLeft.GUID
                                                     join objLeftClass in OntologyList_Classes1 on
                                                         objObjRel.ID_Parent_Object equals objLeftClass.GUID
                                                     join objRight in OntologyList_Classes1 on objObjRel.ID_Other equals
                                                         objRight.GUID
                                                     join objRelationType in OntologyList_RelationTypes1 on
                                                         objObjRel.ID_RelationType equals objRelationType.GUID
                                                     select new clsObjectRel
                                                     {
                                                         ID_Object = objLeft.GUID,
                                                         Name_Object = objLeft.Name,
                                                         ID_Parent_Object = objLeftClass.GUID,
                                                         Name_Parent_Object = objLeftClass.Name,
                                                         ID_Other = objRight.GUID,
                                                         Name_Other = objRight.Name,
                                                         ID_Parent_Other = objRight.GUID_Parent,
                                                         OrderID = objObjRel.OrderID,
                                                         Ontology = objObjRel.Ontology,
                                                         ID_RelationType = objRelationType.GUID,
                                                         Name_RelationType = objRelationType.Name
                                                     }).ToList());

                }

            }

            if (boolIDs)
            {
                return OntologyList_ObjectRel_ID;
            }
            else
            {
                return OntologyList_ObjectRel;
            }

        }

        public long get_Data_RelationTypesCount(List<clsOntologyItem> OList_RelType = null)
        {
            resultRelationTypesCount = null;
            OntologyList_AttributTypes1.Clear();

            var strQuery = create_Query_Simple(OList_RelType, objTypes.RelationType);


            resultRelationTypesCount = ElConnector.Search<clsOntologyItem>(s => s.Index(Index).Type(objTypes.RelationType).Query(q => q.QueryString(qs => qs.Query(strQuery))).From(0).Size(1));

            var lngCount = resultRelationTypesCount.Total;

            return lngCount;
        }

        public List<string> IndexList(string server, int port)
        {
            var uri = new Uri("http://" + server + ":" + port.ToString());

            var settings = new ConnectionSettings(uri);
            var objElConnector = new ElasticClient(settings);
            var dictIndex = objElConnector.CatIndices(q => q.AllIndices());

            return dictIndex.Records.Select(rec => rec.Index).ToList();
        }

        public List<clsOntologyItem> get_Data_RelationTypes(List<clsOntologyItem> OList_RelType = null, bool List2 = false)
        {
            resultRelationTypes = null;
            if (OntologyList_RelationTypes1 == null)
            {
                OntologyList_RelationTypes1 = new List<clsOntologyItem>();
            }

            if (OntologyList_RelationTypes2 == null)
            {
                OntologyList_RelationTypes2 = new List<clsOntologyItem>();
            }

            if (!List2)
            {
                OntologyList_RelationTypes1.Clear();
            }
            else
            {
                OntologyList_RelationTypes2.Clear();
            }


            var strQuery = create_Query_Simple(OList_RelType, objTypes.RelationType);

            var intCount = SearchRange;
            var intPos = 0;

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsOntologyItem>(Indices.Index(Index), Types.Type(objTypes.RelationType));
            searchRequest.Query = queryItem;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;


            while (intCount > 0)
            {
                intCount = 0;

               

                if (scrollId == null)
                {
                    resultRelationTypes = ElConnector.Search<clsOntologyItem>(searchRequest);
                    scrollId = resultRelationTypes.ScrollId;
                }
                else
                {
                    resultRelationTypes = ElConnector.Scroll<clsOntologyItem>("10s", scrollId);
                }
                

                

                if (!List2)
                {
                    OntologyList_RelationTypes1.AddRange(resultRelationTypes.Documents.Select(d => new clsOntologyItem
                    {
                        GUID = d.GUID,
                        Name = d.Name,
                        GUID_Parent = d.GUID_Parent,
                        Type = objTypes.RelationType
                    }));
                }
                else
                {
                    OntologyList_RelationTypes2.AddRange(resultRelationTypes.Documents.Select(d => new clsOntologyItem
                    {
                        GUID = d.GUID,
                        Name = d.Name,
                        GUID_Parent = d.GUID_Parent,
                        Type = objTypes.RelationType
                    }));
                }


                if (resultRelationTypes.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultRelationTypes.Documents.Count();
                    intPos += SearchRange;
                }


            }

            if (!List2)
            {
                return OntologyList_RelationTypes1;
            }
            else
            {
                return OntologyList_RelationTypes2;
            }

        }

        public long get_Data_Rel_OrderByVal(clsOntologyItem OItem_Left,
                                        clsOntologyItem OItem_Right,
                                        clsOntologyItem OItem_RelationType,
                                        string strSort,
                                        bool doASC = true)
        {
            resultObjectRelOrder = null;
            long lngOrderID = 0;
            string strField = strSort;

         
            var strQuery = create_Query_Rel_OrderID(OItem_Left, OItem_Right, OItem_RelationType);

            if (doASC)
            {
                var queryItem = new QueryStringQuery();
                queryItem.Query = strQuery;

                var searchRequest = new SearchRequest<clsObjectRel>(Indices.Index(Index), Types.Type(objTypes.ObjectRel));
                searchRequest.Query = queryItem;
                searchRequest.From = 0;
                searchRequest.Size = 1;
                searchRequest.Sort = new List<ISort>
                {
                    new SortField {Field = strSort, Order = SortOrder.Ascending }
                };

                resultObjectRelOrder = ElConnector.Search<clsObjectRel>(searchRequest);

                if (resultObjectRelOrder.Documents.Any())
                {
                    lngOrderID = (long)resultObjectRelOrder.Documents.First().OrderID;
                }
            }
            else
            {
                var queryItem = new QueryStringQuery();
                queryItem.Query = strQuery;

                var searchRequest = new SearchRequest<clsObjectRel>(Indices.Index(Index), Types.Type(objTypes.ObjectRel));
                searchRequest.Query = queryItem;
                searchRequest.From = 0;
                searchRequest.Size = 1;
                searchRequest.Sort = new List<ISort>
                {
                    new SortField {Field = strSort, Order = SortOrder.Descending }
                };

                resultObjectRelOrder = ElConnector.Search<clsObjectRel>(searchRequest);

                if (resultObjectRelOrder.Documents.Any())
                {
                    lngOrderID = (long)resultObjectRelOrder.Documents.First().OrderID;
                }
            }



            return lngOrderID;
        }

        public long get_Data_Objects_Tree_Count(clsOntologyItem OItem_Class_Par,
                                                clsOntologyItem OItem_Class_Child,
                                                clsOntologyItem OItem_RelationType)
        {
            resultObjectRelTreeCount = null;
            var strQuery = objFields.ID_Parent_Object + ":" + OItem_Class_Par.GUID;
            strQuery += " AND " + objFields.ID_Parent_Other + OItem_Class_Child.GUID;
            strQuery += " AND " + objFields.ID_RelationType + OItem_RelationType.GUID;

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsObjectRel>(Indices.Index(Index), Types.Type(objTypes.ObjectRel));
            searchRequest.Query = queryItem;
            searchRequest.From = 0;
            searchRequest.Size = 1;
            
            resultObjectRelTreeCount = ElConnector.Search<clsObjectRel>(searchRequest);

            var lngCount = resultObjectRelTreeCount.Total;

            return lngCount;
        }

        public List<clsObjectTree> get_Data_Objects_Tree(clsOntologyItem OItem_Class_Par,
                                                clsOntologyItem OItem_Class_Child,
                                                clsOntologyItem OItem_RelationType)
        {
            resultObjectRelTree = null;
            if (OntologyList_ObjectTree == null)
            {
                OntologyList_ObjectTree = new List<clsObjectTree>();
            }

            OntologyList_ObjectTree.Clear();

            var strQuery = objFields.ID_Parent_Object + ":" + OItem_Class_Par.GUID;
            strQuery += " AND " + objFields.ID_Parent_Other + ":" + OItem_Class_Child.GUID;
            strQuery += " AND " + objFields.ID_RelationType + ":" + OItem_RelationType.GUID;

            var objOList_Objects = new List<clsOntologyItem> { new clsOntologyItem { GUID_Parent = OItem_Class_Par.GUID,
                                                                                     Type = objTypes.ObjectType } };

            var objOList_Other = new List<clsOntologyItem> { new clsOntologyItem {GUID_Parent = OItem_Class_Child.GUID,
                                                                                     Type = objTypes.ObjectType } };

            var objOList_RelationType = new List<clsOntologyItem> { OItem_RelationType };


            get_Data_Objects(objOList_Objects);
            get_Data_Objects(objOList_Other, true, false, true);
            get_Data_RelationTypes(objOList_RelationType);

            var intCount = SearchRange;
            var intPos = 0;

            var queryItem = new QueryStringQuery();
            queryItem.Query = strQuery;

            var searchRequest = new SearchRequest<clsObjectRel>(Indices.Index(Index), Types.Type(objTypes.ObjectRel));
            searchRequest.Query = queryItem;
            searchRequest.From = intPos;
            searchRequest.Size = SearchRange;
            searchRequest.Scroll = "10s";

            string scrollId = null;

            while (intCount > 0)
            {
                intCount = 0;
                

                if (scrollId == null)
                {
                    resultObjectRelTree = ElConnector.Search<clsObjectRel>(searchRequest);
                    scrollId = resultObjectRelTree.ScrollId;
                }
                else
                {
                    resultObjectRelTree = ElConnector.Scroll<clsObjectRel>("10s", scrollId);
                }
                

                OntologyList_ObjectTree.AddRange((from objHit in resultObjectRelTree.Documents
                                                  join objLeft in OntologyList_Objects1 on objHit.ID_Object equals objLeft.GUID
                                                  join objRight in OntologyList_Objects2 on objHit.ID_Other equals objRight.GUID
                                                  join objRel in OntologyList_RelationTypes1 on objHit.ID_RelationType equals objRel.GUID
                                                  select new clsObjectTree
                                                  {
                                                      ID_Object = objRight.GUID,
                                                      Name_Object = objRight.Name,
                                                      ID_Parent = objRight.GUID_Parent,
                                                      ID_Object_Parent = objLeft.GUID,
                                                      Name_Object_Parent = objLeft.Name,
                                                      OrderID = objHit.OrderID  ?? 0
                                                  }).ToList().OrderBy(p => p.ID_Object_Parent).ThenBy(p => p.OrderID).ThenBy(p => p.Name_Object).ToList());

                if (resultObjectRelTree.Documents.Count() < SearchRange)
                {
                    intCount = 0;
                }
                else
                {
                    intCount = resultObjectRelTree.Documents.Count();
                    intPos += SearchRange;
                }
            }

            return OntologyList_ObjectTree;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public clsDBSelector(string server,
                          int port,
                          string index,
                          string indexRep,
                          int searchRange,
                          string session)
        {
            this.Server = server;
            this.Port = port;
            this.Index = index;
            this.IndexRep = indexRep;
            this.SearchRange = searchRange;
            this.Session = session;

            SpecialCharacters_Read = new List<string> { "\\", "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":", "@", "/" };
            //SpecialCharacters_Write = new List<string> { ":", "\"" };
            //SpecialCharacters_Read = new List<string> { " ", ":", "/", "\"" };
            initialize_Client();
            sort = SortEnum.NONE;




        }

        

        public clsDBSelector()
        {


    }
    }
}
