﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchNestConnector
{
    [Flags]
    public enum QueryOperator
    {
        AND = 0,
        OR = 1
    }
    public class QueryRow
    {
        public List<QueryItem> QueryItems { get; set; }
        public QueryOperator ItemOperator { get; set; }
        public List<QueryRow> DifferenceRows { get; set; }

        public string RowQuery
        {
            get
            {
                var queryString = "";
                if (QueryItems != null && QueryItems.Any())
                {
                    queryString = "(" + string.Join(" " + Enum.GetName(typeof(QueryOperator), ItemOperator) + " ", QueryItems.Select(queryItem => queryItem.QueryPart)) + ")";
                }
                return queryString;
            }
        }

    }
}
