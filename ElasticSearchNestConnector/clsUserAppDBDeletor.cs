﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using Nest;

namespace ElasticSearchNestConnector
{
    public class clsUserAppDBDeletor
    {
        private clsUserAppDBSelector objUserAppDBSelector;

        private clsLogStates objLogStates = new clsLogStates();
        private clsTypes types = new clsTypes();

        public clsUserAppDBDeletor(clsUserAppDBSelector userAppDBSelector)
        {
            objUserAppDBSelector = userAppDBSelector;
        }

        public clsOntologyItem DeleteItemsById(string index, string type, List<string> idList)
        {
            var items = idList.Select(id => new clsOntologyItem
            {
                GUID = id
            });

            var result = objLogStates.LogState_Success.Clone();

            foreach(var id in idList)
            {
                var deleteRequest = new DeleteRequest(index, type, id);
                var delResult = objUserAppDBSelector.ElConnector.Delete(deleteRequest);
                result = delResult.IsValid ? objLogStates.LogState_Success : objLogStates.LogState_Error;
                if (result.GUID == objLogStates.LogState_Error.GUID)
                {
                    break;
                }
            }



            return result;

        }
    }
}
